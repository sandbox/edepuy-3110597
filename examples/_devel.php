<?php
/**
 * @file
 * Contains development scriptlets.
 */

// Test.
$form['grpt1'] = [
  '#type' => 'details',
  '#title' => 'Test group 1',
  '#open'=> false
];
$form['grpt1']['grpt2'] = [
  '#type' => 'details',
  '#title' => 'Test group 2',
  '#open'=> false
];
$form['grpt1']['testnumber1'] = [
  '#type' => 'number',
  '#title' => 'Some number ',
  '#required' => FALSE,
  '#default_value' => 8,
  '#size' => 5,
  //    '#parents' => 'grpt1'
];
$form['grpt1']['grpt2']['testnumber1'] = [
  '#type' => 'number',
  '#title' => 'Another number ',
  '#required' => FALSE,
  '#default_value' => 299,
  '#size' => 5,
  //    '#parents' => 'grpt1'
];


//    $form['information'] = array(
//      '#type' => 'vertical_tabs',
//      '#default_tab' => 'edit-publication',
//    );
//      $form['author'] = array(
//        '#type' => 'details',
//        '#title' => $this
//          ->t('Author'),
//        '#group' => 'information',
//      );
//        $form['author']['name'] = array(
//          '#type' => 'textfield',
//          '#title' => $this
//            ->t('Name'),
//        );
//      $form['publication'] = array(
//        '#type' => 'details',
//        '#title' => $this
//          ->t('Publication'),
//        '#group' => 'information',
//      );
//        $form['publication']['publisher'] = array(
//          '#type' => 'textfield',
//          '#title' => $this
//            ->t('Publisher'),
//        );

