<?php

/**
 * @file
 *
 * This module:
 *  Creates a settings form. The field schema is defined in a single .yml file.
 *  Supports all FAPI form controls and elements; see: https://api.drupal.org/api/drupal/elements/8.2.x
 *
 * The .yml file:
 *  Defines field schema's for the settings fields.
 *  Can be placed anywhere. Preferably outside the webroot.
 *  Example folders: //:private, ../settings.
 *
 * Example yml schema's are included. See ./examples.
 *
 * Basic field-input validation:
 *  Use the pattern element! See: https://www.w3schools.com/tags/att_input_pattern.asp
 *
 */

namespace Drupal\site_settings_ymlform\form;

use Drupal;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Exception;
use Symfony\Component\Yaml\Yaml;
use site_settings_ymlform;
use Drupal\user\Entity\User;

/**
 * Implements the SimpleForm form controller.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class SettingsForm extends FormBase {

  const FORMFIELD_SCHEMA_RELATIVEPATH = 'form-schema-relativepath';
  const FILENAME_SETTINGS_SCHEMA = 'site_settings_ymlform.schema.yml';

  /**
   * Getter method for Form ID.
   *
   * The form ID is used in implementations of hook_form_alter() to allow other
   * modules to alter the render array built by this form controller. It must be
   * unique site wide. It normally starts with the providing module's name.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId() {
    return 'site_settings_ymlform_form';
  }

  /**
   * {@inheritdoc}
   */
  protected static function getModuleName() {
    return site_settings_ymlform::$moduleName;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      self::getModuleName() . '.' . 'settings',
    ];
  }

  /**
   * Build the form.
   *
   * A build form method constructs an array that defines how markup and
   * other form elements are included in an HTML form.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Instert module settings.
    $config = Drupal::config($this->getEditableConfigNames()[0]);
    $schema_filepath = $this->getSchemaFilePath();
    $schema_contents = file_get_contents($this->getSchemaFilePath());

    $form['intro'] = [
      '#type' => 'item',
      '#title' => '',
      '#tree' => FALSE,
      '#weight' => -9999999,
      '#markup' => $this->t('Use this page to manage custom site settings.'),
    ];
    $user = Drupal::currentUser();
    if ($user->hasPermission('site_settings_ymlform administer schema')) {
      $form['schema'] = [
        '#type' => 'details',
        '#title' => 'Settings schema',
        '#weight' => -9999998,
        '#open'=> false
      ];
      $form['schema'][self::FORMFIELD_SCHEMA_RELATIVEPATH] = [
        '#type' => 'textfield',
        '#title' => $this->t('Relative path to folder containing the custom site settings schema (@schema)', ['@schema' =>self::FILENAME_SETTINGS_SCHEMA]),
        '#description' => t('Folder examples: //:private, ../settings,  or any custom folder outside the webroot.'),
        '#weight' => -9999997,
        '#default_value' => $config->get($this->getModuleName() . '.' . self::FORMFIELD_SCHEMA_RELATIVEPATH),
        '#required' => TRUE,
        '#disabled' => !$user->hasPermission('site_settings_ymlform administer schema'),
      ];
      $form['schema']['description'] = [
        '#type' => 'item',
        '#title' => '',
        '#tree' => FALSE,
        '#markup' => $this->t('* Make sure this file exists in the location you entered below. <br>
          * It is not smart to save this file in a public folder! <br>
          * Also make sure this file is added to your version control system.'),
      ];
      $form['markup'] = [
        '#type' => 'item',
        '#markup' => '<hr><h3>' . t('Settings') . '</h3>',
      ];
    }
    // -------------------------------------------------------------------------

    // SCHEMA YML
    // Read and render the custom settings schema.
    if ($schema_contents) {
      $formItems = Yaml::parse($schema_contents);
      $items = reset($formItems)['mapping'];
      // Scan schema items.
      foreach ($items as $itemId => $itemData) {
        $ItemFapi = [];

        // Calculate default_value(s). From schema or from stored.
        $yml_default_value = isset($itemData['default_value']) ? $itemData['default_value'] : NULL;
        $storedValue = $config->get($this->getModuleName() . '.' . $itemId);
        $default_value = isset($storedValue) ? $storedValue : $yml_default_value;
        $default_values = '';

        // Special cases, duhhh
        switch ($itemData['type']) {
          case 'checkboxes':
          case 'select':
            // Read defaults_value array from yml.
            $yml_default_values = isset($itemData['default_value']) ? $itemData['default_value'] : [];
            // Extract only checked items from stored list.
            $stored_list = $config->get($this->getModuleName() . '.' . $itemId);
            $checked_items = array_diff($stored_list, ["0"]);
            $default_values = isset($checked_items) ? $checked_items : $yml_default_values;
            break;
          case 'radios':
            // Seems already OK. Because multiple values are not allowed.
            break;
          case 'managed_file':
            $stored_fid = $config->get($this->getModuleName() . '.' . $itemId);
            $default_value = isset($stored_fid) ? $stored_fid : NULL;
            break;
          default:
            break;
        }

        // Build the FAPI render array.
        $ItemFapi['#tree'] = FALSE; //!! makes save() a lot easier.
        $parents = '';
        foreach ($itemData as $key => $keyval) {
          switch ($key) {
            case 'parents':
              $parents = explode(' ',$itemData['parents'] . ' ' . $itemId);
              break;
            default:
              $ItemFapi['#' . $key] = $keyval;
              break;
            case 'default_value':
              if ($key == 'default_value') {
                // Special cases for default_value and checked state.
                switch ($itemData['type']) {
                  case 'checkboxes':
                  case 'select':
                    $ItemFapi['#default_value'] = $default_values;
                    break;
                  case 'checkbox':
                    $ItemFapi['#default_value'] = ($default_value == TRUE);
                    break;
                  case 'radio' :
                    $ItemFapi['#attributes'] = ['checked' => ($default_value == TRUE)];
                    break;
                  default:
                    $ItemFapi['#default_value'] = $default_value;
                    break;
                }
              }
              break;
          }
        }

        // Add rendered item to form
        if ($parents) {
          // Handle nested items.
          $s='';
          foreach ($parents as $p){
            if ($p != $itemId) {
              $s .= '["'.trim($p).'"]';
            }
          }
          $f = '$form' . $s . '["$itemId"] = $ItemFapi;';
          eval($f);

        } else {
          $form[$itemId] = $ItemFapi;
        }
    }

   } else {
      $this->messenger()->addWarning('Schema file not found or empty: ' . $schema_filepath);
    }

    // Group submit handlers in an actions element with a key of "actions" so
    // that it gets styled correctly, and so that other modules may add actions
    // to the form. This is not required, but is convention.
    $form['actions'] = [
      '#type' => 'actions',
    ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    // Add Reset button.
    //    $form['actions']['reset'] = [
    //      '#type' => 'button',
    //      '#value' => 'Reset',
    //      '#attributes' => ['onclick' => 'this.form.reset(); return true;'],
    //    ];
    $form['actions']['cancel'] = [
      '#type' => 'submit',
      '#value' => 'Cancel',
      '#attributes' => ['onclick' => 'this.form.reset(); return true;'],
      '#limit_validation_errors' => []
    ];

    return $form;
  }

  /**
   * Implements form validation.
   *
   * The validateForm method is the default method called to validate input on
   * a form.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /**
     * use the pattern element! See: https://www.w3schools.com/tags/att_input_pattern.asp
     *
     */
    //parent::validateForm($form, $form_state);
    //$form_state->setErrorByName('some_elementId', $this->t('This input is not valid.'));
    $d=0;
  }

  /**
   * Implements a form submit handler.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
      switch ($form_state->getTriggeringElement('#id')['#type']) {
      case 'submit':
        $this->Settings_SubmitHandler($form, $form_state);
        break;
      case 'cancel':
        $this->Settings_CancelHandler($form, $form_state);
        break;
    }
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function Settings_SubmitHandler(array &$form, FormStateInterface $form_state) {
    $config = Drupal::service('config.factory')->getEditable($this->getEditableConfigNames()[0]);

    // Set and save module config.
    try {
      $config->set($this->getModuleName() . '.'. self::FORMFIELD_SCHEMA_RELATIVEPATH, $form_state->getValue(self::FORMFIELD_SCHEMA_RELATIVEPATH));
      $config->save();
    } catch (Exception $e) {
      $msg = $this->t('There were errors saving the settings Path!') . ': ' . $e->getMessage();
      $this->messenger()->addError(substr($msg, 0, 500));
      Drupal::Logger($this->getModuleName())->error($msg);
    }

    // Set and save form inputs.
    $schema_contents = file_get_contents($this->getSchemaFilePath());
    if ($schema_contents) {
      $formItems = Yaml::parse($schema_contents);
      $items = reset($formItems)['mapping'];
        try {
        foreach ($items as $itemId => $itemData) {
          // Filter only actual fields (ie. no details, fieldset, vertical_tab, table_select).
          if (!in_array($itemData['type'], ['details', 'fieldset', 'vertical_tab', 'table_select'])) {
            try {
              $config->set($this->getModuleName() . '.' . $itemId, $form_state->getValue($itemId));
              $config->save();
            } catch (Exception $e) {
              $msg = $this->t('There were errors saving the custom site settings Path!') . ': ' . $e->getMessage();
              $this->messenger()->addError(substr($msg, 0, 500));
              Drupal::Logger($this->getModuleName())->error($msg);
            }
          }
        }
        $this->messenger()->addMessage($this->t('Settings saved OK.'));
      } catch (Exception $e) {
        $msg = $this->t('There were errors saving the site settings Path!') . ': ' . $e->getMessage();
        $this->messenger()->addError(substr($msg,0, 500));
        Drupal::Logger($this->getModuleName())->error($msg);
      }
    } else {
      $msg = t('File not found: @file', ['@file' => $this->getSchemaFilePath()]) ;
      $this->messenger()->addError($msg);
      Drupal::Logger($this->getModuleName())->error($msg);
    }

  }

  /**
   * Implements a Cancel handler.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function Settings_CancelHandler(array &$form, FormStateInterface $form_state) {
    $gotoUrl = url::fromRoute('system.admin_config_system');
    $form_state->setRedirectUrl($gotoUrl);
  }

  /**
   * Get real filepath of the ymlfile containing the settings form fields.
   * Folderpath is defined in the module setting (/config/install/MODULENAME.settings.yml)
   * Filename is defined in constant self::FILENAME_SETTINGS_SCHEMA
   *
   * @return string
   */
  protected function getSchemaFilePath() {
    $config = Drupal::config($this->getEditableConfigNames()[0]);
    $schema_folder_path = !empty($config->get($this->getModuleName() . '.'
      . self::FORMFIELD_SCHEMA_RELATIVEPATH)) ? $config->get($this->getModuleName() . '.'
      . self::FORMFIELD_SCHEMA_RELATIVEPATH) : null;

    return Drupal::service('file_system')
          ->realpath($schema_folder_path) . '/' . self::FILENAME_SETTINGS_SCHEMA;
  }

}
