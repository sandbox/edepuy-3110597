Custom Site Settings ymlForm
1 feb 2020

INTRODUCTION
------------
Provides a custom site settings form created from a single .yml file. No code required!
Supports all FAPI form controls and elements; see: https://api.drupal.org/api/drupal/elements/8.2.x
Also support nested form elements! Examples are included.

REQUIREMENTS
------------

No special requirements.



INSTALLATION
------------

* Install as you would normally install a contributed Drupal module.
  See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------
1. After installation goto /admin/structure/site_settings_ymlform/settings and enter a folderpath for your schema file.
   The schema file (site_settings_ymlform.schema.yml) defines the schema for all the form fields.
   The yml file can be placed anywhere. Smartest is to place it in a non-public folder.
   Example folders: //:private, ../settings,  or any custom folder outside the webroot.

2. Create/Modify your site_settings_ymlform.schema.yml and place it in the folderpath you entered in step 1.
   The schema syntax must comply to the drupal Forms API (FAPI).
   See: https://api.drupal.org/api/drupal/elements/8.2.x

3. Tip: For field input validation: use the pattern element!
   See: https://www.w3schools.com/tags/att_input_pattern.asp


EXAMPLES
--------

Examples are in folder /examples.


MAINTAINERS
-----------

Current maintainers:
 * Edgar de Puy - https://www.drupal.org/u/edepuy

